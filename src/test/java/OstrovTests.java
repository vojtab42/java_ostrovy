import org.junit.Test;

import java.util.Set;
import java.util.TreeSet;

import static org.junit.Assert.*;

public class OstrovTests {

    @Test
    public void vytvoreniPrazdnehoOstrovu() {
        Ostrov o = new Ostrov();
        Set<String> predpoklad = new TreeSet<String>();

        assertEquals(predpoklad,o.getUzly());

    }

    @Test
    public void vytvoreniOstrovuSJednimUzlem () {
        String jmenoOstrovu = "ostruvek";
        Ostrov o = new Ostrov(jmenoOstrovu);
        Set<String> predpoklad = new TreeSet<String>();
        predpoklad.add(jmenoOstrovu);

        assertEquals(predpoklad,o.getUzly());
    }

    @Test
    public void pricteniOstrovuKOstrovu() {
        Ostrov o1 = new Ostrov("o1");
        Ostrov o2 = new Ostrov("o2");


        Set<String> predpoklad = new TreeSet<String>();
        predpoklad.add("o1");
        predpoklad.add("o2");

        o1.prictiOstrov(o2);

        assertEquals(predpoklad,o1.getUzly());


    }

    @Test
    public void toStringVypis() {
        Ostrov zkusebni = new Ostrov("o1");
        Ostrov zkusebni2 = new Ostrov("o2");
        zkusebni.prictiOstrov(zkusebni2);

        zkusebni.toString();


    }
}
