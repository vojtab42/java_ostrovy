import java.util.ArrayList;
import java.util.List;


public class Ostrov implements Comparable<Ostrov> {

    private List<String> uzly;

    Ostrov(){
        uzly=new ArrayList<>();
    }

    Ostrov(String uzel){
        uzly = new ArrayList<>();
        uzly.add(uzel);
    }

    Ostrov(List<String> uzly){
        this.uzly=uzly;
    }

    public boolean obsahujeUzel(String uzel){
        return uzly.contains(uzel);
    }

    public List<String> getUzly() {
        return uzly;
    }

    public void prictiOstrov (Ostrov o1){
        uzly.addAll(o1.getUzly());
    }



    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        for (String uzel:uzly) {
            s.append(uzel + ", ");
        }
        s.delete(s.length()-2, s.length());
        return s.toString();
    }

    @Override
    public int compareTo(Ostrov o) {
        return uzly.size() - o.getUzly().size();
    }
}
