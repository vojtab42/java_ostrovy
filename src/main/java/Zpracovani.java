import java.util.ArrayList;
import java.util.List;
import java.util.Collection;
import java.util.TreeSet;

public class Zpracovani implements IZpracovani{

    private List<Ostrov> ostrovy;
    private List<String> uzly;
    private TreeSet<String[]> hrany;
    private INacteniSouboru nacteniSouboru;


    public Zpracovani(INacteniSouboru nacteniSouboru) {
        this.nacteniSouboru = nacteniSouboru;

        //v ramci testu
        ostrovy = new ArrayList<Ostrov>();
        uzly = new ArrayList<String>();

    }

    @Override
    public void setUzly(List<String> s) {

    }

    @Override
    public void setHrany(Collection c) {

    }

    @Override
    public void zpracuj() {
        String uzelHrany1;
        String uzelHrany2;



        for (String[] hran:hrany){
            uzelHrany1 = hran[0];
            uzelHrany2 = hran[1];
            Ostrov ostrov1=null;

            for (Ostrov o : ostrovy) {

                for (String s : o.getUzly()) {

                    if (s == uzelHrany1 || s == uzelHrany2) {

                        if (ostrov1!=null)
                        {
                            if(o!=ostrov1){
                                ostrov1.prictiOstrov(o);
                                ostrovy.remove(o);
                            }else{
                                System.out.println("hrana "+uzelHrany1+" - "+uzelHrany2+" netvori novy ostrov" );
                                //doimplementovat rozšíření projektu o rozepínání hran
                            }

                        }
                        else {
                            ostrov1=o;
                        }
                    }
                }


            }
        }

    }

    @Override
    public int getPocetOstrovu() {
        return 0;
    }

    @Override
    public Collection getUzlyOstrovu() {
        return null;
    }

    public List<String> getUzly() {
        return uzly;
    }
}

