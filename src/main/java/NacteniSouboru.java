import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Scanner;


public class NacteniSouboru implements INacteniSouboru {

    private ArrayList<String> uzly;
    private ArrayList<String[]> hrany;
    private InputStream vstup;


    NacteniSouboru(InputStream vstup){
        uzly = new ArrayList<>();
        hrany = new ArrayList<String[]>();
        this.vstup= vstup;
    }

    @Override
    public ArrayList<String> getUzly() {
        return uzly;
    }

    @Override
    public ArrayList<ArrayList<String>> getHrany() {
        return null;
    }



    @Override
    public void nacti() {
        Scanner scan = new Scanner(vstup);
        //uzly = new TreeSet<String>();
        //hrany = new TreeSet<Set<String>>();

        String radek;
        boolean ctuUzly = true;

        while (scan.hasNextLine()) {
            radek = scan.nextLine().trim();
            if (radek.length() == 0) {
                ctuUzly = false;
            }
            if (ctuUzly == true) {


                // otestuj na pritomnost carky
                if (radek.contains(",")) {
                    System.err.println("je tam carka");
                    System.exit(1);
                }

                // otestuj na pritomnost v mnozine
                if (uzly.contains(radek)) {
                    System.err.println("vstup obsahuje 2x stejny uzel");
                    System.exit(1);
                }
                // pridej do mnoziny
                uzly.add(radek);
            }
            else // ctu hrany
            {
                // test 1 carka
                if (!radek.contains(",") || (radek.indexOf(",") != radek.lastIndexOf(","))) {
                    System.err.println("vstup Hran neobsahuje carku nebo je jich vice");
                    System.exit(1);
                }
                String[] hrana = new String[2];
                String temp;
                temp = radek.substring(0, radek.indexOf(","));

                hrana[1]=temp;
                temp = radek.substring(radek.indexOf(","), radek.length());
                hrana[2]=temp;

                // pred a za carkou jsou uzly z mnoziny

                if (!uzly.contains(hrana[0])||!uzly.contains(hrana[1])) {
                    System.err.println("Hrana obsahuje uzly ktere nebyly nacteny");
                    System.exit(1);
                }
                // pridej hranu
                hrany.add(hrana);
            }

            System.exit(0); // ukonceni programu
        }

    }

}