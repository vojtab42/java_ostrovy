import java.io.File;
import java.util.ArrayList;
import java.util.Set;

public interface INacteniSouboru {

    void nacti();
    ArrayList<String> getUzly();
    ArrayList<ArrayList<String>>getHrany();


}
